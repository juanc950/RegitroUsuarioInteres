package com.example.juancarlos.appusuario;

import android.os.Bundle;
import android.support.v4.view.accessibility.AccessibilityNodeInfoCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by juancarlos on 29/07/2017.
 */

public class Interes extends AppCompatActivity{
    RadioButton et1,et2,et3,et4,et5,et6;
    Button enviodato;
    RadioGroup Android1, Java2, Spring3;
    private ListView lv1;
    private List<String> listado;
    private ArrayAdapter<String> adapter;
    String valor1,valor2,valor3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interes);
        enviodato = (Button)findViewById(R.id.butEnviar);



        Android1 = (RadioGroup)findViewById(R.id.opcion1);
        Java2 = (RadioGroup)findViewById(R.id.opcion2);
        Spring3 = (RadioGroup)findViewById(R.id.opcion3);

        listado = new ArrayList<>();
        lv1= (ListView) findViewById(R.id.lv1 );
        adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,listado);
        lv1.setAdapter(adapter);

        enviodato.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {

                if (Android1.getCheckedRadioButtonId() == R.id.anSI) {
                    valor1 = "- Android -";
                } else if (Android1.getCheckedRadioButtonId() == R.id.anNo) {
                    valor1 = "";
                }
                if (Java2.getCheckedRadioButtonId() == R.id.jaSI) {
                    valor2 = "- Java -";
                } else if (Java2.getCheckedRadioButtonId() == R.id.jaNo) {
                    valor2 = "";
                }
                if (Spring3.getCheckedRadioButtonId() == R.id.spSI) {
                    valor3 = "- Spring -";
                } else if (Spring3.getCheckedRadioButtonId() == R.id.spNo) {
                    valor3 = "";
                }
                if (Android1.getCheckedRadioButtonId() == R.id.anNo && Java2.getCheckedRadioButtonId() == R.id.jaNo &&Spring3.getCheckedRadioButtonId() == R.id.spNo) {
                    Toast.makeText(getApplicationContext(),"Usted ha seleccionado a todos no",Toast.LENGTH_SHORT).show();
                    listado.add("Ninguna");
                } else {
                    Toast.makeText(getApplicationContext(),"Usted ha seleccionado a todos no"+ valor1 + valor2 + valor3,Toast.LENGTH_SHORT).show();
                    listado.add(valor1 + valor2 + valor3);
                }
                adapter.notifyDataSetChanged();
                Android1.clearCheck();
                Java2.clearCheck();
                Spring3.clearCheck();

            }
        });


    }
}
