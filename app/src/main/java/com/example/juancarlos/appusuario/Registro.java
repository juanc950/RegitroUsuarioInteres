package com.example.juancarlos.appusuario;

import android.content.Intent;
import android.graphics.Region;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by juancarlos on 29/07/2017.
 */

public class Registro extends AppCompatActivity{

    EditText nombre, apellido, contrasena1, contrasena2;
    Button butAceptarregistro,butCanselarRegistro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        butAceptarregistro = (Button) findViewById(R.id.butAceptar);
        butCanselarRegistro = (Button) findViewById(R.id.butCancelar);
        nombre = (EditText) findViewById(R.id.etnombre);
        apellido = (EditText)findViewById(R.id.etapellido);
        contrasena1 = (EditText)findViewById(R.id.etcontra1);
        contrasena2 = (EditText)findViewById(R.id.etcontra2);

        butAceptarregistro.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (nombre.getText().toString().equals("")||apellido.getText().toString().equals("")||contrasena1.getText().toString().equals("")||contrasena2.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"Campos estan sin datos",Toast.LENGTH_SHORT).show();
                }


                else
                    {
                        if (contrasena1.getText().toString().equals(contrasena2.getText().toString()))
                        {
                            Intent generarEncuesta2 = new Intent(Registro.this,Interes.class);
                            startActivity(generarEncuesta2);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"las contraseña no son iguales",Toast.LENGTH_SHORT).show();
                        }

                }

            }
        });

        butCanselarRegistro.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                finish();
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

    }
}
